const express = require("express");
const app = express();
const PORT = 3000;
//build small REST API with Express

console.log("Server-side program starting...");

//baseurl: http://localhost:3000/
// Endpoint: http://localhost:3000/
app.get('/', (req, res) => {
    res.send('Hello world');
});

/**
 * This arrow function adds two numbers together
 * @param {number} a first param
 * @param {number} b second param
 * @returns {number}
 */
const add = (a, b) => {
    const a1 = parseInt(a);
    const b1 = parseInt(b);
    const sum = a1 + b1;
    return sum;
};

//Adding endpoint: http://localhost:3000/add?a=value&b=value
app.get('/add', (req, res) => {
    console.log(req.query);
    const sum = add(req.query.a, req.query.b);
    res.send(sum.toString());
})

app.listen(PORT, () => console.log(
    `Server listening http://localhost:${PORT}`
));
